# imports
import concurrent.futures
import gc
import sys
import time
import io


class number_Obj:
    value = None
    row_index = None
    column_index = None
    pre_number = None

    right_number = None
    left_number = None
    lower_number = None
    upper_numer = None

    def __init__(self, value, row_index, column_index, predecessor):
        self.value = value
        self.row_index = row_index
        self.column_index = column_index
        self.pre_number = predecessor


debug = 2

Numbers_to_search = []
Numbers_to_search_2 = []

Prohibited_numbers = []

Not_Addition = []
Not_Substract = []
Not_Multiply = []
Not_Divison = []

future_Not_Substract = []
future_Not_Divison = []

calculated_number = []
visited_number = []
future_visited_number = []

number_front = []
number_back = []
number_back_finish = []
other_number = []

coordinate_for_formula = []

max_column = 5
max_row = 6
starttime = time.time()


def test_of_already_visit(operator_number, number, modus):
    column = number.column_index
    row = number.row_index
    # Addition 1
    if modus == 1:
        column = column + 1
    # Subtraktion - 1
    elif modus == -1:
        column = column - 1
    # Multiplikation 2
    elif modus == 2:
        row = row + 1
    # Division - 2
    elif modus == -2:
        row = row - 1

    for n in visited_number:
        if operator_number == n.value and n.row_index == row and n.column_index == column:
            return True

    return False


def test_number(number):
    for prohibited_number in Prohibited_numbers:
        if prohibited_number == number: return -1
    for search_number in Numbers_to_search:
        if search_number == number:
            Numbers_to_search.remove(number)
            break
    return 0


def addition_number(value):
    value = value + 1
    solve_test = test_number(value)

    return value if solve_test == 0 else -1


def substract_number(value):
    value = value - 1
    solve_test = test_number(value)

    return value if solve_test == 0 else -1


def multiply_number(value, factor):
    value = value * factor
    solve_test = test_number(value)
    return value if solve_test == 0 else -1


def divide_number(value, divisor):
    if value % divisor == 0:
        value = value / divisor
    else:
        return -1

    solve_test = test_number(value)
    return int(value) if solve_test == 0 else -1


def __thread_calc_number(list_of_numbers: list, operator_modus: int) -> list:
    finished = False
    list_add = []
    list_sub = []
    list_mul = []
    list_div = []

    list_future_not_sub = []
    list_future_not_div = []

    # //modus: -1-> without Plus 1-> without minus -2-> without division 2 -> without multiply If the field is later
    # identified as too small, possible initial numbers are collected in advance for the new size.
    while len(list_of_numbers) > 0:
        number = list_of_numbers[0]
        list_of_numbers.remove(number)
        if operator_modus != -1:
            # Path of addition
            if number.column_index + 1 <= max_column:
                add_number = addition_number(number.value)
                if test_of_already_visit(add_number, number, 1): add_number = -1
                if add_number > 0:
                    new_number = number_Obj(add_number, number.row_index, number.column_index + 1, number)
                    visited_number.append(new_number)
                    number.right_number = new_number
                    new_number.left_number = number
                    calculated_number.append(new_number)
                    write_Numbers_in_Data(number, new_number)
                    if len(Numbers_to_search) == 0:
                        finished = True
                        break
                    list_add.append(new_number)

            else:
                add_number = addition_number(number.value)
                if test_of_already_visit(add_number, number, 1): add_number = -1
                if add_number > 0:
                    new_number = number_Obj(add_number, number.row_index, number.column_index + 1, number)
                    future_visited_number.append(new_number)
                    number.right_number = new_number
                    new_number.left_number = number
                    list_future_not_sub.append(new_number)

        if operator_modus != 1:
            # Path of substract
            if number.column_index - 1 > 0:
                sub_number = substract_number(number.value)
                if test_of_already_visit(sub_number, number, -1): sub_number = -1
                if sub_number > 0:
                    new_number = number_Obj(sub_number, number.row_index, number.column_index - 1, number)
                    visited_number.append(new_number)
                    number.left_number = new_number
                    new_number.right_number = number
                    calculated_number.append(new_number)
                    write_Numbers_in_Data(number, new_number)
                    if len(Numbers_to_search) == 0:
                        finished = True
                        break
                    list_sub.append(new_number)

        if operator_modus != -2:
            # Path of multiplikation
            if number.row_index + 1 <= max_row:
                mul_number = multiply_number(number.value, number.row_index + 1)
                if test_of_already_visit(mul_number, number, 2): mul_number = -1
                if mul_number > 0:
                    new_number = number_Obj(mul_number, number.row_index + 1, number.column_index, number)
                    visited_number.append(new_number)
                    number.lower_number = new_number
                    new_number.upper_number = number
                    calculated_number.append(new_number)
                    write_Numbers_in_Data(number, new_number)
                    if len(Numbers_to_search) == 0:
                        finished = True
                        break
                    list_mul.append(new_number)

            else:
                mul_number = multiply_number(number.value, number.row_index + 1)
                if test_of_already_visit(mul_number, number, 2): mul_number = -1
                if mul_number > 0:
                    new_number = number_Obj(mul_number, number.row_index + 1, number.column_index, number)
                    future_visited_number.append(new_number)
                    number.lower_number = new_number
                    new_number.upper_number = number
                    list_future_not_div.append(new_number)

        if operator_modus != 2:
            # Path of division
            if number.row_index - 1 > 0:
                div_number = divide_number(number.value, number.row_index)
                if test_of_already_visit(div_number, number, -2): div_number = -1
                if div_number > 0:
                    new_number = number_Obj(div_number, number.row_index - 1, number.column_index, number)
                    visited_number.append(new_number)
                    number.upper_number = new_number
                    new_number.lower_number = number
                    calculated_number.append(new_number)
                    write_Numbers_in_Data(number, new_number)
                    if len(Numbers_to_search) == 0:
                        finished = True
                        break
                    list_div.append(new_number)

    combined_list = [list_add, list_sub, list_mul, list_div, list_future_not_sub,
                     list_future_not_div] if not finished else None

    return combined_list


def __read_in_out_of_file_numbers(modus):
    global Numbers_to_search

    data_name = 'input/search' if modus == 0 else 'input/without'

    try:
        with io.open(data_name, 'r', encoding='utf8') as f:
            lines = f.readlines()

            for line in lines:
                number_in_Line = line.split(" ")
                for number in number_in_Line:
                    if modus == 0:
                        Numbers_to_search.append(int(number))
                        Numbers_to_search_2.append(int(number))

                    if modus == 1:
                        Prohibited_numbers.append(int(number))

    except FileNotFoundError:
        print('The file \'search\' not found') if modus == 0 else print('The file \'without\' not found')
        exit(-1)


def write_Numbers_in_Data(from_number: number_Obj, to_number: number_Obj):
    s = " | search number: "
    if debug > 1:
        for n in Numbers_to_search:
            s += "{0} ".format(n)
    if debug > 0:
        endtime = time.time()
        output_flow__file_numbers = "{0}-> {1}{2}; It took {3} seconds\n".format(from_number.value, to_number.value, s,
                                                                                 (endtime - starttime))
        output_flow__file_coardinates = "r: {0}, c: {1})-> r: {2}, c: {3})\n".format(from_number.row_index,
                                                                                     from_number.column_index,
                                                                                     to_number.row_index,
                                                                                     to_number.column_index)
        with io.open('output/Flow_file_numbers', 'a', encoding='utf8') as Flow_file_numbers, \
                io.open('output/Flow_file_coardinates', 'a', encoding='utf8') as Flow_file_coardinates:
            Flow_file_numbers.write(output_flow__file_numbers)
            Flow_file_coardinates.write(output_flow__file_coardinates)


def calculate_number_out():
    while len(Not_Addition) > 0 or len(Not_Substract) > 0 or len(Not_Multiply) > 0 or len(Not_Divison) > 0:

        # '''
        # Multithreading:

        with concurrent.futures.ThreadPoolExecutor() as executor:
            minion_not_sub = executor.submit(__thread_calc_number, Not_Substract, 1)
            minion_not_div = executor.submit(__thread_calc_number, Not_Divison, 2)
            minion_not_add = executor.submit(__thread_calc_number, Not_Addition, -1)
            minion_not_mul = executor.submit(__thread_calc_number, Not_Multiply, -2)

            if minion_not_sub.result() is not None and minion_not_div.result() is not None and minion_not_add.result() is not None and minion_not_mul.result() is not None:
                Not_Substract.clear()
                Not_Substract.extend(minion_not_sub.result()[0])
                Not_Substract.extend(minion_not_div.result()[0])
                Not_Substract.extend(minion_not_add.result()[0])
                Not_Substract.extend(minion_not_mul.result()[0])

                Not_Addition.clear()
                Not_Addition.extend(minion_not_sub.result()[1])
                Not_Addition.extend(minion_not_div.result()[1])
                Not_Addition.extend(minion_not_add.result()[1])
                Not_Addition.extend(minion_not_mul.result()[1])

                Not_Divison.clear()
                Not_Divison.extend(minion_not_sub.result()[2])
                Not_Divison.extend(minion_not_div.result()[2])
                Not_Divison.extend(minion_not_add.result()[2])
                Not_Divison.extend(minion_not_mul.result()[2])

                Not_Multiply.clear()
                Not_Multiply.extend(minion_not_sub.result()[3])
                Not_Multiply.extend(minion_not_div.result()[3])
                Not_Multiply.extend(minion_not_add.result()[3])
                Not_Multiply.extend(minion_not_mul.result()[3])

                future_Not_Substract.extend(minion_not_sub.result()[4])
                future_Not_Substract.extend(minion_not_div.result()[4])
                future_Not_Substract.extend(minion_not_add.result()[4])
                future_Not_Substract.extend(minion_not_mul.result()[4])

                future_Not_Divison.extend(minion_not_sub.result()[5])
                future_Not_Divison.extend(minion_not_div.result()[5])
                future_Not_Divison.extend(minion_not_add.result()[5])
                future_Not_Divison.extend(minion_not_mul.result()[5])

            else:
                return 1

    return 0
    # '''

    '''
        # Test without Threading
        minion_not_sub = __thread_calc_number(Not_Substract, 1)
        minion_not_div = __thread_calc_number(Not_Divison, 2)
        minion_not_add = __thread_calc_number(Not_Addition, -1)
        minion_not_mul = __thread_calc_number(Not_Multiply, -2)

        if minion_not_sub is not None and minion_not_div is not None and minion_not_add is not None and minion_not_mul is not None:

            Not_Substract.extend(minion_not_sub[0])
            Not_Substract.extend(minion_not_div[0])
            Not_Substract.extend(minion_not_add[0])
            Not_Substract.extend(minion_not_mul[0])

            Not_Addition.extend(minion_not_sub[1])
            Not_Addition.extend(minion_not_div[1])
            Not_Addition.extend(minion_not_add[1])
            Not_Addition.extend(minion_not_mul[1])

            Not_Divison.extend(minion_not_sub[2])
            Not_Divison.extend(minion_not_div[2])
            Not_Divison.extend(minion_not_add[2])
            Not_Divison.extend(minion_not_mul[2])

            Not_Multiply.extend(minion_not_sub[3])
            Not_Multiply.extend(minion_not_div[3])
            Not_Multiply.extend(minion_not_add[3])
            Not_Multiply.extend(minion_not_mul[3])

            future_Not_Substract.extend(minion_not_sub[4])
            future_Not_Substract.extend(minion_not_div[4])
            future_Not_Substract.extend(minion_not_add[4])
            future_Not_Substract.extend(minion_not_mul[4])

            future_Not_Divison.extend(minion_not_sub[5])
            future_Not_Divison.extend(minion_not_div[5])
            future_Not_Divison.extend(minion_not_add[5])
            future_Not_Divison.extend(minion_not_mul[5])

        else:
            return 1

    return 0
    '''


def construct_from_given_path_a_formula():
    row_split = []
    brackets_split = []
    formula_set = []
    number_of_Formular = None
    with io.open('output/Path_to_Number', 'r', encoding='utf8') as file:
        Lines = file.readlines()

    for line in Lines:
        if not ':' in line:
            continue
        elif not 'Coordinates' in line:
            row_split = line.split(':')
            number_of_Formular = row_split[0][0:-1]
        else:
            line = line[14:]
            row_split = line.split('-> ')

            if ('\n') in row_split[-1]:
                row_split[-1] = row_split[-1].replace('\n', '')

            for brackets in row_split:
                bracket = []
                brackets_split = brackets.split(',')
                brackets_split[0] = brackets_split[0][1:]
                brackets_split[1] = brackets_split[1][1:-1]
                bracket = [brackets_split[0], brackets_split[1]]

                coordinate_for_formula.append(bracket)

            formula = 'x'
            current_pos = coordinate_for_formula[0]
            considered_pos = None
            accu = 0

            for k in coordinate_for_formula:
                considered_pos = k
                if considered_pos[1] > current_pos[1]:
                    accu += 1
                elif considered_pos[1] < current_pos[1]:
                    accu -= 1
                elif accu != 0 and considered_pos[1] == current_pos[1]:
                    if accu>0:
                        formula += '+' + str(accu)
                    else:
                        formula += str(accu)
                    accu = 0

                if considered_pos[0] > current_pos[0]:
                    formula = "({0})*{1}".format(formula, considered_pos[0])
                if considered_pos[0] < current_pos[0]:
                    formula = "({0})/{1}".format(formula, current_pos[0])
                current_pos = considered_pos

            if accu > 0:
                formula += '+' + str(accu)
            elif accu<0:
                formula += str(accu)
            accu = 0


            formula = "{0}: {1}".format(number_of_Formular, formula)
            formula_set += formula + '\n'
            coordinate_for_formula.clear()

    with io.open('Formula_Set', 'w', encoding='utf8') as file:
        for form in formula_set:
            file.write(form)
        file.close()


def __search_path(max_n: int):
    global max_row
    global max_column
    path_found = False

    # read the file 'search' and 'without' and put all numbers in the correct list
    __read_in_out_of_file_numbers(0)
    __read_in_out_of_file_numbers(1)

    # delete numbers in 'search', which is also in 'without'
    for numbervalue in Prohibited_numbers:
        if numbervalue in Numbers_to_search:
            Numbers_to_search.remove(numbervalue)
            Numbers_to_search_2.remove(numbervalue)

    for i in range(max_n):

        max_row = i + 1
        max_column = i + 2

        print("max_row is {0}, max_column is {1}".format(max_row, max_column))

        if i != 0:
            for future_number in future_visited_number:
                calculated_number.append(future_number)
                write_Numbers_in_Data(future_number.pre_number, future_number)

                future_visited_number.clear()

            Not_Substract.extend(future_Not_Substract)
            Not_Divison.extend(future_Not_Divison)
            future_Not_Substract.clear()
            future_Not_Divison.clear()

        if i == 0:
            init_number = number_Obj(-1, -1, -1, None)
            number = number_Obj(1, 1, 1, init_number)

            visited_number.append(number)
            Not_Substract.append(number)
            calculated_number.append(number)
            write_Numbers_in_Data(init_number, number)

        if calculate_number_out() != 1:
            with io.open('output/Flow_file_numbers', 'a', encoding='utf8') as Flow_file_numbers, \
                    io.open('output/Flow_file_coardinates', 'a', encoding='utf8') as Flow_file_coardinates:
                Flow_file_numbers.write('||| No path found |||')
                Flow_file_coardinates.write('||| No path found |||')
                Flow_file_coardinates.close()
                Flow_file_numbers.close()

            continue

        with io.open('output/Flow_file_numbers', 'a', encoding='utf8') as Flow_file_numbers, \
                io.open('output/Flow_file_coardinates', 'a', encoding='utf8') as Flow_file_coardinates:

            Flow_file_numbers.write('||| Path found |||')
            Flow_file_coardinates.write('||| Path found |||')
            Flow_file_coardinates.close()
            Flow_file_numbers.close()
        path_found = True
        break

    if not path_found:
        exit(-10)

    Ffn = "For a {0} x {1} table, there exists at least one path to all numbers searched that are not forbidden.\n".format(
        max_row, max_column)

    current_number = None
    for j in range(len(calculated_number)):
        if calculated_number[j].value == Numbers_to_search_2[0]:
            current_number = calculated_number[j]
            break
    next_number = None

    List_of_numbers = []
    index_of_last_viewed_number = -1
    count_of_list = 0

    while not len(Numbers_to_search_2) == 0:
        next_number = current_number
        if next_number.value == -1: break

        if current_number.value in Numbers_to_search_2:
            Numbers_to_search_2.remove(current_number.value)
        index_of_last_viewed_number = calculated_number.index(current_number)
        while index_of_last_viewed_number < len(calculated_number):
            Path_number = []
            calculated_number.remove(current_number)
            Path_number.append(current_number)
            while current_number.pre_number is not None:
                current_number = current_number.pre_number
                if current_number.pre_number is None: break
                Path_number.append(current_number)
            List_of_numbers.append(Path_number)
            count_of_list = count_of_list + 1
            tmp = next_number.value
            for n in calculated_number:
                if n.value == tmp:
                    current_number = n
                    break

            if current_number in calculated_number:
                index_of_last_viewed_number = calculated_number.index(current_number)
            else:
                index_of_last_viewed_number = -1

            if current_number.value < 0 or index_of_last_viewed_number == -1:
                index_of_last_viewed_number = len(calculated_number)

        path_number = List_of_numbers[0]
        for path in List_of_numbers:
            if len(path_number) == 0:
                path_number = path
                continue
            if len(path) > 0 and (len(path) < len(path_number)): path_number = path

        List_of_numbers.clear()
        Ffn += "{0} : ".format(path_number[0].value)
        path_number.reverse()
        for i in range(len(path_number)):
            Ffn += "{0}".format(path_number[i].value)
            if i + 1 < len(path_number): Ffn += "-> "

        Ffn += "\nCoordinates : "
        for i in range(len(path_number)):
            Ffn += "({0}, {1})".format(path_number[i].row_index, path_number[i].column_index)
            if i + 1 < len(path_number): Ffn += "-> "
        Ffn += "\n({0} - Steps required )\n".format(len(path_number))
        path_number.clear()
        if len(Numbers_to_search_2) > 0:
            tmp = Numbers_to_search_2[0]
            for n in calculated_number:
                if n.value == tmp:
                    current_number = n
                    break

    timeend = time.time()
    Ffn += "It took {0} seconds\n".format(timeend - starttime)  # time/1000000000.0 maybe
    with io.open('output/Path_to_Number', 'a', encoding='utf8') as Flow_file_numbers, \
            io.open('output/Flow_file_coardinates', 'a', encoding='utf8') as Flow_file_coardinates:
        Flow_file_numbers.write(Ffn)
        Flow_file_numbers.close()
    construct_from_given_path_a_formula()


def __main():
    if len(sys.argv) == 2:

        #construct_from_given_path_a_formula()
        __search_path(int(sys.argv[1]))

    else:
        print('invalid numbers of arguments!')
        exit(-1)


if __name__ == '__main__':
    __main()
